#!/usr/bin/python

import os
import sys
import getopt
import subprocess
import xml.etree.ElementTree

def main(argv):
    opts, args = getopt.getopt(argv,"hc:")
    for opt, arg in opts:
        if opt == "-c":
            config = arg

    print 'Using "' + config + '"'
    print 'Processing repositories:'

    root = xml.etree.ElementTree.parse(config).getroot()
    for repository in root.iter('repository'):
        svn = repository.find('svn')
        if svn is not None:
            auth = svn.find('auth')
            url = svn.get('url')
            if auth is not None:
                username = auth.get('username')
                password = auth.get('password')
                devnull = open(os.devnull, 'w')
                info = subprocess.call('svn info ' + url + '@HEAD --non-interactive --username ' + username + ' --password ' + password, shell=True, stdout=devnull, stderr=devnull)
                if info == 0:
                    print '[DONE]', url
                else:
                    print '[FAIL]', url
            else:
                print '[SKIP]', url, '(no auth)'


if __name__ == "__main__":
    main(sys.argv[1:])
