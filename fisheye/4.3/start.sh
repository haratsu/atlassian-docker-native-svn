#!/bin/bash

echo "Configuring..." && sh configure.sh
echo "configuring security ..." && python3 ./set_svn_passwords.py -c /atlassian/data/fisheye/config.xml
echo "Starting..." && sh bin/run.sh
