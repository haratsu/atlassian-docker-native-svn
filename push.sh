#!/bin/bash

echo "Pushing atlassian-base..." && docker push "mswinarski/atlassian-base"
echo "Pushing atlassian-fisheye:4.2..." && docker push "mswinarski/atlassian-crucible:4.2"
echo "Pushing atlassian-crucible:4.2..." && docker push "mswinarski/atlassian-fisheye:4.2"
echo "Pushing atlassian-fisheye:latest..." && docker push "mswinarski/atlassian-crucible:latest"
echo "Pushing atlassian-crucible:latest..." && docker push "mswinarski/atlassian-fisheye:latest"
