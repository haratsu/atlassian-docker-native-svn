#!/bin/bash

echo "Building atlassian-base..." && docker build -t "mswinarski/atlassian-base:1.8" ./base/1.8/

echo "Building atlassian-fisheye 4.2..." && docker build -t "mswinarski/atlassian-fisheye:4.2" ./fisheye/4.2/
echo "Building atlassian-crucible 4.2..." && docker build -t "mswinarski/atlassian-crucible:4.2" ./crucible/4.2/
echo "Building atlassian-fisheye latest..." && docker build -t "mswinarski/atlassian-fisheye:latest" ./fisheye/4.2/
echo "Building atlassian-crucible latest..." && docker build -t "mswinarski/atlassian-crucible:latest" ./crucible/4.2/
