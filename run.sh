#!/bin/bash

echo "Stopping fisheye1..." && docker stop fisheye1
echo "Removing fisheye1..." && docker rm fisheye1
echo "Starting fisheye1..." && docker run -d -p 8081:8080 --name fisheye1 mswinarski/atlassian-fisheye:latest

echo "Stopping crucible1..." && docker stop crucible1
echo "Removing crucible1..." && docker rm crucible1
echo "Starting crucible1..." && docker run -d -p 8082:8080 --name crucible1 mswinarski/atlassian-crucible:latest

echo "Running containers..." && docker ps
echo ""
echo "------------- Docker help -------------"
echo "run and ssh: docker run -it mswinarski/atlassian-fisheye:latest /bin/bash"
echo "ssh to fisheye1: docker exec -it fisheye1 /bin/bash"
echo "remove all containers: docker stop \$(docker ps -a -q) && docker rm \$(docker ps -a -q)"
