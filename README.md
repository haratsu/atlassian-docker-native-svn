# Fisheye with native SVN and SVN auth #

This fork of [/mswinarski/atlassian-docker](https://bitbucket.org/mswinarski/atlassian-docker).
Version: Fisheye 4.3


# Setup
## create a directory to place fisheye data
    export EXT_DATA_DIR=/home/fisheye/data
    mkdir -p $EXT_DATA_DIR
## create a directory to place svn authentication cache 
    export SVN_AUTH_CACHE=/home/fisheye/svn
    mkdir -p $SVN_AUTH_CACHE
## run the container using volumes to make containerinternal data accessible
    docker run -d -p 9090:8080 -v $EXT_DATA_DIR:/atlassian/data/fisheye \
    -v $SVN_AUTH_CACHE:/root/.subversion/auth/svn.simple \ 
    --name fisheyeMultiRepo \
    -e 'FISHEYE_OPTS=-Dfecru.configure.from.env.variables=true -Xmx30G -Xms30G' atlassian-docker-native-svn